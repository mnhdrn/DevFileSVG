#!/bin/bash
# Taper "./langstat.sh dico.txt --p" dans la console pour utilisé l'autre
# paramètre.
# Le packet "bc" doit être installé pour réaliser les calculs en Bash et donc
# calculer les Pourcentages.

alpha=('A' 'B' 'C' 'D' 'E' 'F' 'G' 'H' 'I' 'J' 'K' 'L' 'M' 'N' 'O' 'P' 'Q' 'R' 'S' 'T' 'U' 'V' 'W' 'X' 'Y' 'Z')
i=0
sum=0
test=0

if [ -z "$1" ] # Vérifie qu'un document est spécifié.
then
  echo "Vous devez donner comme argument un document comportant des lettres !!!"
else
  FILE=$1
  if [ ! -f "$FILE" ] # Vérifie que le document spécifié existe.
  then
    echo "$FILE n'existe pas !!!"
  else
    if [ -z "$2" ] # Vérifie que le second paramètre est vide
    then # Calcule le nombre de lettre dans le doc'
      echo '';
      echo "Nombre de lettre dans le document :"
      while [ $i -le 25 ]
      do
        letter=${alpha[$i]};
        echo "$(grep -a $letter $1 | wc -l) - $letter"
        i=$(($i + 1))
      done | sort -nr
    else # Calcule les lettres du doc' en pourcentages
      echo '';
      echo "Pourcentage de lettre dans le document :"
      while [ $i -le 25 ]
      do
        letter=${alpha[$i]};
        grep -o $letter $1 | wc -l >> tmp
        i=$(($i + 1))
      done;
      # Calcul la somme de toute les lettres
      while read num
      do
        sum=$(($sum + $num))
      done < tmp;
      # Réalise un produit en croix pour obtenir un Pourcentages
      i=0
      while read -r line
      do
        letter=${alpha[$i]};
        multi=$(($line * 100))
        divide=$(bc -l <<< "scale=2; $multi / $sum" | awk '{printf "%.2f", $0}' );
        echo "$divide %  - $letter ";
        i=$(($i + 1));
      done < tmp
      rm tmp; # Efface le fichier temporaire.
    fi
  fi
fi
