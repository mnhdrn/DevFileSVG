//======================================================
const contElt = document.getElementById('contenu');
const formElt = document.querySelector('form');
const inputElt = document.getElementById('searcher');

var linkInfo = document.createElement('span');
var nameInfo = document.createElement('h3');

var profilPic = document.createElement('img');
profilPic.src = "";
profilPic.style.width = "20 %";

contElt.appendChild(profilPic);
contElt.appendChild(nameInfo);
contElt.appendChild(linkInfo);

formElt.addEventListener("submit", function (e) {
    var IdIn = inputElt.value;
    var GitUrl = `https://api.github.com/users/${IdIn}`
    var regex = /https:\/\/api\.github\.com\/users\/.+/

    if (regex.test(GitUrl))
    {
      var infoApi;

      ajaxGet(GitUrl,function (response){

        infoApi = JSON.parse(response);
        console.log(infoApi)

        profilPic.src = infoApi.avatar_url;
        linkInfo.textContent = infoApi.blog;
        nameInfo.textContent = infoApi.name;

        return (infoApi);
      });
    }
    else {
      console.log("error")
    }

    e.preventDefault();
});
