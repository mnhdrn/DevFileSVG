//------------------------ Create DOM Basic Element
//
//
$('body').append($('<div id="header"></div>'));
$('body').append($(`<div class="butStyle">Clique Ici !!!</div>`));

//------------------------ Global Var Init
//
//
const echo = console.log;
const header = $("#header");
const buttonOne = $(".butStyle");
const contentMessage = ["J","O","Y","E","U","X"," ",
"A","N","N","I","V","E","R","S","A","I","R","E"]

//------------------------ Function Init
//
//
const createMessage = () => {
  let i;

  i = 0;
  while (i < contentMessage.length)
  {
    let delayMessage = (i * 100) + 600;

    var container = $(`<span>${contentMessage[i]}</span>`);
    container.css('padding-right', "0.5vw");
    container.hide().delay(delayMessage).fadeIn(delayMessage)
    header.append(container);
    i++;
  }
  return;
};

const createCommentOne = () => {
  let comment = $("<div></div>");
  var content = "T'es le meilleur Francis !!!";
  comment.addClass('butStyle commentOne');
  comment.css('width','40vw');
  comment.css('height','25vw');
  comment.css('margin-left','8vw');
  comment.css('margin-top','27vh');
  comment.css('font-size','2em');
  comment.css('line-height','25vw');
  comment.text(content);
  comment.hide().delay(1500).fadeIn(1500);
  $('body').append(comment);

  $('.commentOne').on('click', () => {
    $("body").css('background-image', `url('assets/pandi.svg')`);
    $("body").css('background-size', 'auto 52vw');
    $("body").css('background-position', '2vw -1.2vh');
    $('.commentOne').fadeOut(1000);
    createCommentTwo();
  });
  return;
};

const createCommentTwo = () => {
  let comment = $("<div></div>");
  var content = "Tu aime les chapeaux !!!";
  comment.addClass('butStyle commentTwo');
  comment.css('width','40vw');
  comment.css('height','25vw');
  comment.css('margin-left','48vw');
  comment.css('margin-top','27vh');
  comment.css('font-size','2em');
  comment.css('line-height','25vw');
  comment.text(content);
  comment.hide().delay(1500).fadeIn(1500);
  $('body').append(comment);

  $('.commentTwo').on('click', () => {
    $("body").css('background-image', `url('assets/cowcow.svg')`);
    $("body").css('background-size', 'cover');
    $('.commentTwo').fadeOut(1000);
    createCommentThree();
  });
  return;
};

const createCommentThree = () => {
  let comment = $("<div></div>");
  var content = "Et tu vas souffler tes bougies";
  comment.addClass('butStyle commentThree');
  comment.addClass('butStyle commentOne');
  comment.css('width','40vw');
  comment.css('height','25vw');
  comment.css('margin-left','8vw');
  comment.css('margin-top','27vh');
  comment.css('font-size','2em');
  comment.css('line-height','25vw');
  comment.text(content);
  comment.hide().delay(1500).fadeIn(1500);
  $('body').append(comment);

  $('.commentThree').on('click', () => {
    let audio = $(`<audio><source src="assets/horn.mp3"></source></audio>`);

    $('body').append(audio);
    $('.commentThree').fadeOut(1000);
    $("body").css('background-image', `url('assets/gato.svg')`);
    $("body").css('background-position','0 -15vh');
    $("audio").get(0).play();
  });
  return;
};
//------------------------ Basic Event Init
//
//
buttonOne.hide().delay(3200).fadeIn(2000);

buttonOne.on('click', () => {
  buttonOne.fadeOut(800).remove();
  header.css('margin-top', '5vh');
  $("body").css('background-image', `url('assets/LOL.svg')`);
  $("body").css('background-position', 'right top');
  $("body").css('background-size', 'auto 50vw');
  $("body").css('background-color', '#487396');
  createCommentOne();
  return;
});

//------------------------ First Function Launch
//
//
createMessage();
