/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               GLOBAL INIT                                 */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const flur  = console.log;
const mapHTML   = document.getElementById('map');
const but_attack = document.getElementById('but_attack');
const but_move = document.getElementById('but_move');
const but_defend = document.getElementById('but_defend');
const but_turn = document.getElementById('but_turn');
const notifier = document.getElementById('Notifier');
var isoInitTop,
isoInitLeft,
mapWidth,
mapHeight,
getButAtt,
getButMv,
getButDef,
getButTrn,
timeId1;
var isMap = [];
var but_actived = false;
var abortTurn = false;
var playerOne = {
  name: "playerOne",
  notif: "Player-One",
  vie : 100,
  pos: 0,
  arme: "Woodstick",
  degat: 10,
  defend: 0,
  defCount: 0,
  tour: 0,
  moveRange: 3,
  attackRange: 1,
};
var playerTwo = {
  name: "playerTwo",
  notif: "Player-Two",
  vie : 100,
  Pos: 0,
  arme: "Woodstick",
  degat: 10,
  defend: 0,
  defCount: 0,
  tour: 0,
  moveRange: 3,
  attackRange: 1,
};
var weapType = {
  w1: "Woodstick",
  w2: "Sword",
  w3: "Shuriken",
  w4: "Spear",
};
var game = {
  turnNumber: 0,
  count: 1,
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                              BUTTON ACTION                                */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
//- - -  Attack Button
but_attack.addEventListener('click', () => {
  if (but_actived === false)
  {
    if (game.turnNumber === 1)
    {
      attackPartOne(playerOne, playerTwo);
    };
    if (game.turnNumber === 2)
    {
      attackPartOne(playerTwo, playerOne);
    };
    but_actived = true;
    activedButton(but_attack, getButAtt);
  }
  else {
    but_actived = false;
    eventCanceller();
  };
});

//*****************************************************************************
//- - -  Move Button
but_move.addEventListener('click', () => {
  if (but_actived === false)
  {
    if (game.turnNumber === 1)
    {
      movePartOne(playerOne);
    }
    if (game.turnNumber === 2)
    {
      movePartOne(playerTwo);
    };
    but_actived = true;
    activedButton(but_move, getButMv);
  }
  else {
    but_actived = false;
    eventCanceller();
  };
});

//*****************************************************************************
//- - -  Defend Button
but_defend.addEventListener('click', () => {
  if (but_actived === false)
  {
    if (game.turnNumber === 1)
    {
      defendAction(playerOne);
    }
    if (game.turnNumber === 2)
    {
      defendAction(playerTwo);
    }
    activedButton(but_defend, getButDef);
    but_actived = true;
  }
  else {
    but_actived = false;
    eventCanceller();
  };
});

//*****************************************************************************
//- - -  Turn Button
but_turn.addEventListener('click', () => {
  activedButton(but_turn, getButTrn);
  setTimeout(() => {
    abortTurn = true;
  }, 250);
  return;
});


/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                              BUTTON ACTION                                */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const activedButton = (button, value) => {
  var getValue = Number(value.replace(/px/g,''));

  button.style.marginTop = getValue + (0.5 * vd) + 'px';

  timeId1 = setTimeout(() => {
    button.style.marginTop = value;
  }, 180);
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             GET MAP Function                              */
/*                                                                           */
/* - - - Get Map Status                                                      */
/* ************************************************************************* */
const getMap = () => {
  let iX = 0, iY = 0;

  isMap = [];
  while(iY < 10)
  {
    iX = 0;
    while(iX < 10)
    {
      isMap.push(mapHTML.rows[iY].cells[iX]);
      iX++;
    };
    iY++;
  };
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                            GET PLAYER POSITION                            */
/*                                                                           */
/* - - - Return Player Pos                                                   */
/* ************************************************************************* */
const getPos = (player) => {
  isMap.forEach((e, i)=> {
    if (e.hasChildNodes())
    {
      if (e.childNodes[0].id === player.name)
      return player.pos = i;
    };
  });
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             GET CLASS Function                            */
/*                                                                           */
/* - - - hasClass function                                                   */
/* ************************************************************************* */
const hasClass = (el, cls) => {
  return (' ' + el.className + ' ').indexOf(' ' + cls + ' ') > -1;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               GET CELLS                                   */
/*                                                                           */
/* - - - Collect cells from a range in array before activation of event      */
/* ************************************************************************* */
const getCellsTop = (player, range, yAxis, actCells, stop) => {
  let i;

  i = 1;
  while ((player.pos - i) > (player.pos - (range + 1)))
  {
    if (!(typeof isMap[player.pos - (i * 10)] === 'undefined'))
    {
      if (hasClass(isMap[player.pos - (i * 10)], 'rocked') ||
      (stop === true && hasClass(isMap[player.pos - (i * 10)], 'Player')))
      {
        i = player.pos + (range + 1);
      }
      else if ((player.pos - range) > yAxis)
      {
        actCells.splice(0, 0, isMap[player.pos - (i * 10)]);
        i++;
      }
      else {
        i++;
      };
    }
    else {
      i = player.pos + (range + 1);
    };
  };
  return (actCells);
};

//*****************************************************************************
const getCellsDown = (player, range, yAxis, actCells, stop) => {
  let i;

  i = 1;
  while ((player.pos + i) < (player.pos + (range + 1)))
  {
    if (!(typeof isMap[player.pos + (i * 10)] === 'undefined'))
    {
      if (hasClass(isMap[player.pos + (i * 10)], 'rocked') ||
      (stop === true && hasClass(isMap[player.pos + (i * 10)], 'Player')))
      {
        i = player.pos + (range + 1);
      }
      else if ((player.pos + range) < (yAxis + 90))
      {
        actCells.splice(0, 0, isMap[player.pos + (i * 10)]);
        i++;
      }
      else {
        i++;
      };
    }
    else {
      i = player.pos + (range + 1);
    };
  };
  return (actCells);
};

//*****************************************************************************
const getCellsLeft = (player, range, xAxis, actCells, stop) => {
  let i;

  i = 1;
  while ((player.pos - i) > (player.pos - (range + 1)))
  {
    if (!(typeof isMap[player.pos - i] === 'undefined'))
    {
      if (hasClass(isMap[player.pos - i], 'rocked') ||
      (stop === true && hasClass(isMap[player.pos - i], 'Player')))
      {
        i = player.pos + (range + 1);
      }
      else if ((player.pos - i) > (xAxis - 1))
      {
        actCells.splice(0, 0, isMap[player.pos - i]);
        i++;
      }
      else {
        i++;
      };
    }
    else {
      i = player.pos + (range + 1);
    };
  };
  return (actCells);
};

//*****************************************************************************
const getCellsRight = (player, range, xAxis, actCells, stop) => {
  let i;

  i = 1;
  while ((player.pos + i) < (player.pos + (range + 1)))
  {
    if (!(typeof isMap[player.pos + i] === 'undefined'))
    {
      if (hasClass(isMap[player.pos + i], 'rocked') ||
      (stop === true && hasClass(isMap[player.pos + i], 'Player')))
      {
        i = player.pos + (range + 1);
      }
      else if ((player.pos + i) < (xAxis + 10))
      {
        actCells.splice(0, 0, isMap[player.pos + i]);
        i++;
      }
      else {
        i++;
      };
    }
    else {
      i = player.pos + (range + 1);
    };
  };
  return (actCells);
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             ATTACK Function                               */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const attackPartOne = (player, victim) => {
  var range = player.attackRange;
  var xAxis = Math.floor(player.pos/10)*10;
  var yAxis = Number((player.pos + 10).toString()[1]);
  var actCells = [];

  getCellsTop(player, range, yAxis, actCells, false);
  getCellsDown(player, range, yAxis, actCells, false);
  getCellsLeft(player, range, xAxis, actCells, false);
  getCellsRight(player, range, xAxis, actCells, false);

  isMap.forEach((e, i) => {
    if (!(typeof actCells[i] === 'undefined'))
    {
      if (!(hasClass(actCells[i], 'rocked')))
      {
        actCells[i].classList.add('attack');
        actCells[i].onclick = (e) => attackPartTwo(player, victim, e.target);
      };
    };
  });
  return;
};

//*****************************************************************************
const attackPartTwo = (player, victim, clickPos) => {
  if (clickPos.id === victim.name)
  {
    if (victim.defend === 0)
    {
      notifier.textContent = `${player.notif} attack ${victim.notif}
      and ${victim.notif} lose ${player.degat} PV`;
      victim.vie -= player.degat;
    }
    else {
      notifier.textContent = `${player.notif} attack ${victim.notif}
      and ${victim.notif} lose ${player.degat/2} PV`;
      victim.vie -= (player.degat/2);
    }
    notifAction();
    // notif end of turn ++ change turn
    setTimeout(() => {
      abortTurn = true;
    }, 1180);
    return eventCanceller();
  }
  else {
    notifier.textContent = `${player.notif} attack missed...`;
    notifAction();
    // notif end of turn ++ change turn
    setTimeout(() => {
      abortTurn = true;
    }, 1180);
    return eventCanceller();
  };
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             MOVE Function                                 */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const movePartOne = (player) => {
  var range = player.moveRange;
  var xAxis = Math.floor(player.pos/10)*10;
  var yAxis = Number((player.pos + 10).toString()[1]);
  var actCells = [];

  getCellsTop(player, range, yAxis, actCells, true);
  getCellsDown(player, range, yAxis, actCells, true);
  getCellsLeft(player, range, xAxis, actCells, true);
  getCellsRight(player, range, xAxis, actCells, true);

  isMap.forEach((e, i) => {
    if (!(typeof actCells[i] === 'undefined'))
    {
      if (!(hasClass(actCells[i], 'rocked')))
      {
        actCells[i].classList.add('move');
        actCells[i].onclick = (e) => movePartTwo(player, e.target);
      };
    };
  });
  return;
};

//*****************************************************************************
const movePartTwo = (player, clickPos) => {
  if (hasClass(clickPos, 'weapon'))
  {
    changeWeaponPlayer(player, clickPos);
  }
  removePlayer(player);
  getMap();
  posPlayer(player, clickPos);
  eventCanceller();
  setTimeout(() => {
    abortTurn = true;
  }, 300);
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                         EVENT CANCELLER Function                          */
/*                                                                           */
/* - - - Cancel Move and Attack Cells activation                             */
/* ************************************************************************* */
const eventCanceller = () => {
  isMap.forEach((e, i) => {
    if (!(hasClass(e,'rocked')))
    {
      if (hasClass(e,'attack'))
      {
        e.onclick = "null";
        e.classList.remove('attack');
      };

      if (hasClass(e,'move'))
      {
        e.onclick = "null";
        e.classList.remove('move');
      };
    };
  });
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                         DEFEND ACTION Function                            */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const defendAction = (player) => {
  var sourceCount = game.count + 1;

  notifier.textContent = `${player.notif} defense is actived !!!`;
  notifAction();
  player.defend = 1;
  player.defCount = sourceCount;
  setTimeout(() => {
    abortTurn = true;
  }, 1180);
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                         CHECK DEFEND Function                             */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const checkDefend = (player) => {
  if (game.count == player.defCount)
  {
    player.defend = 0;
    flur("defence is over");
  }
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               INIT ROCK                                   */
/*                                                                           */
/* - - - Initiate rock position                                              */
/* ************************************************************************* */
const setRock = () => {
  var randomNumber = Math.floor((Math.random() * 18) + 2)
  let i = 0;
  var rockPos;

  while (i < randomNumber)
  {
    rockPos = Math.floor(Math.random() * 99);
    if (hasClass(isMap[rockPos], 'rocked'))
    {
      rockPos = Math.floor(Math.random() * 99);
      i--;
    }
    else {
      isMap[rockPos].classList.add('rocked');
    };
    i++;
  };
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               INIT WEAPON                                 */
/*                                                                           */
/* - - - Initiate weapon position                                            */
/* ************************************************************************* */
const setWeapon = () => {
  let i = 0;
  let nb = 0;
  var weapPos;

  while (i < 4)
  {
    weapPos = Math.floor(Math.random() * 99);
    if (hasClass(isMap[weapPos], 'rocked') ||
    typeof isMap[weapPos] === "undefined")
    {
      i--;
    }
    else if (nb < 4)
    {
      isMap[weapPos].classList.add('weapon');
      nb++;
      i++;
    }
    else
    {
      i++;
    }
  };
  return setTypeWeapon();
};

//*****************************************************************************
const setTypeWeapon = () => {
  var weapons = [];
  let size = 0;
  let i= 0;

  isMap.forEach((e, i) => {
    if (hasClass(e, 'weapon'))
    {
      size++;
      weapons.splice(0, 0, i);
    }
  });

  while (i < size)
  {
    if (typeof isMap[weapons[i]] !== "undefined")
    {
      isMap[weapons[i]].classList.add(`weap${i+1}`);
      flur(isMap[weapons[i]])
    };
    i++;
  };
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               Change WEAPON                               */
/*                                                                           */
/* - - - Change weapon value when player walk on it                          */
/* ************************************************************************* */
const changeWeaponPlayer = (player, clickPos) => {
  flur("Weapon changed");

  if (hasClass(clickPos,"weap1"))
  {
    changeImgWeapon('weapImg1', player);
    player.arme = weapType.w1;
    player.attackRange = 1;
    player.degat = 10;
    changeWeaponType(player, clickPos, "weap1");
  }
  else if (hasClass(clickPos,"weap2"))
  {
    changeImgWeapon('weapImg2', player);
    player.arme = weapType.w2;
    player.attackRange = 1;
    player.degat = 15;
    changeWeaponType(player, clickPos, "weap2");
  }
  else if (hasClass(clickPos,"weap3"))
  {
    changeImgWeapon('weapImg3', player);
    player.arme = weapType.w3;
    player.attackRange = 2;
    player.degat = 6;
    changeWeaponType(player, clickPos, "weap3");
  }
  else if (hasClass(clickPos,"weap4"))
  {
    changeImgWeapon('weapImg4', player);
    player.arme = weapType.w4;
    player.attackRange = 1;
    player.degat = 20;
    changeWeaponType(player, clickPos, "weap4");
  }
  else
  {
    changeWeaponType(player, clickPos, "weap1");
    player.arme = weapType.w1;
    player.attackRange = 1;
    player.degat = 10;
    changeImgWeapon('weapImg1', player);
  }
  notifier.textContent = `${player.name} weapon is ${player.arme} now.`;
  notifAction();
  return (uiInfo());
};

//*****************************************************************************
const changeWeaponType = (player, clickPos, value) => {
  if (player.arme == weapType.w1)
  {
    flur(player.arme +' '+ weapType.w1)
    clickPos.classList.remove(value);
    clickPos.classList.add("weap1");
  }
  else if (player.arme == weapType.w2)
  {
    clickPos.classList.remove(value);
    clickPos.classList.add("weap2");
  }
  else if (player.arme == weapType.w3)
  {
    clickPos.classList.remove(value);
    clickPos.classList.add("weap3");
  }
  else if (player.arme == weapType.w4)
  {
    clickPos.classList.remove(value);
    clickPos.classList.add("weap4");
  };
  return;
};

//*****************************************************************************
const changeImgWeapon = (sprite, player) => {
  var elementSelect = document.getElementById(sprite);

  if (player.arme == weapType.w1)
  {
    elementSelect.src ='./assets/wood.SVG';
  }
  else if (player.arme == weapType.w2)
  {
    elementSelect.src ='./assets/sword.SVG';
  }
  else if (player.arme == weapType.w3)
  {
    elementSelect.src ='./assets/shuriken.SVG';
  }
  else if (player.arme == weapType.w4)
  {
    elementSelect.src ='./assets/spear.SVG';
  };
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                          SET PLAYER Function                              */
/*                                                                           */
/* - - - Initiate player position                                            */
/* ************************************************************************* */
const setPlayer = (player) => {
  var randomNumber = Math.floor(Math.random() * 99);
  var playerElement = document.createElement('div');
  playerElement.id = `${player.name}`;

  if (hasClass(isMap[randomNumber], 'rocked') ||
  hasClass(isMap[randomNumber], 'weapon'))
  {
    setPlayer(player);
  }
  else {
    isMap[randomNumber].appendChild(playerElement);
    isMap[randomNumber].classList.add('Player');
    getMap();
    getPos(player);
  };
  return;
};


/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                        REMOVE PLAYER Function                             */
/*                                                                           */
/* - - - REMOVE A PLAYER                                                     */
/* ************************************************************************* */
const removePlayer = (player) => {
  var elPos = document.getElementById(`${player.name}`).parentNode;
  if (!(typeof elPos === 'undefined'))
  {
    elPos.classList.remove('Player');
    elPos.removeChild(elPos.childNodes[0]);
  };
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                        POSITION PLAYER Function                           */
/*                                                                           */
/* - - - Initiate player class position                                      */
/* ************************************************************************* */
const posPlayer = (player, posTarget) => {
  var playerElement = document.createElement('div');
  playerElement.id = `${player.name}`;

  if (!(hasClass(isMap[player.pos], 'rocked')))
  {
    posTarget.appendChild(playerElement);
    getMap();
    getPos(player);
    isMap[player.pos].classList.add('Player');
  };
  autoImgPos(player);
  return getPos(player);
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                        CHECK INIT PLAYER POSITION                         */
/*                                                                           */
/* - - - Check player position                                               */
/* ************************************************************************* */
const checkStartPos = (playerOne, playerTwo) => {
  var randomNumber = Math.floor(Math.random() * 59)+30;

  getPos(playerOne);
  getPos(playerTwo);
  if (!(playerOne.pos === playerTwo.pos))
  {
    if (playerOne.pos > (playerTwo.pos + randomNumber))
    {
      return flur("Game Is Ready");
    }
    else {
      reSetPlayer(playerOne);
      reSetPlayer(playerTwo);
      checkStartPos(playerOne, playerTwo);
      return;
    };
  }
  else {
    reSetPlayer(playerOne);
    reSetPlayer(playerTwo);
    checkStartPos(playerOne, playerTwo);
    return;
  };
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                        Re INIT POSITION PLAYER                            */
/*                                                                           */
/* - - - Reposition Player                                                   */
/* ************************************************************************* */
const reSetPlayer = (player) => {
  getPos(player);
  removePlayer(player);
  setPlayer(player);
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             ACTIVE PLAYER                                 */
/*                                                                           */
/* - - - player is active                                                    */
/* ************************************************************************* */
const getActived = (player) => {
  document.getElementById(player.name).classList.add('active');
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                            SET RANDOM TURN                                */
/*                                                                           */
/* - - - set random Turn                                                     */
/* ************************************************************************* */
const setTurn = (game) => {
  var random = Math.floor((Math.random() * 2) + 1);

  game.turnNumber = random;
  if (random === 1)
  return getActived(playerOne);
  if (random === 2)
  return getActived(playerTwo);
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                           SET TURN Function                               */
/*                                                                           */
/* - - - set turn                                                            */
/* ************************************************************************* */
const doTurn = () => {
  var timeZone = document.getElementById('zone_time');
  var timeCount = 60;
  var countInt;

  countInt = setInterval(() => {
    if (abortTurn == true)
    {
      timeCount = -1;
      abortTurn = false;
    }
    if (timeCount < 0)
    {
      clearInterval(countInt);
      changeTurn(game);
      return;
    }
    else
    {
      timeZone.textContent = `${timeCount}`;
      timeCount--;
    }
  },1000);
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                           CHANGE TURN Function                            */
/*                                                                           */
/* - - - change turn                                                         */
/* ************************************************************************* */
const changeTurn = (game) => {
  eventCanceller();

  if (game.turnNumber == 1)
  {
    game.turnNumber = 2;
    document.getElementById('playerOne').classList.remove('active');
    if (playerTwo.vie <= 0)
    {
      return endGame('Player-One');
    }
    else
    {
      notifier.textContent = `It's time for Player-Two to play !`;
      doTurn();
      getActived(playerTwo);
      checkDefend(playerTwo);
      initMonitor();
      uiInfo();
      notifAction();
    };
    return game.count++;
  };

  if (game.turnNumber == 2)
  {
    game.turnNumber = 1;
    document.getElementById('playerTwo').classList.remove('active');
    if (playerOne.vie <= 0)
    {
      return endGame('Player-Two');
    }
    else
    {
      notifier.textContent = `It's time for Player-One to play !`;
      doTurn();
      getActived(playerOne);
      checkDefend(playerOne);
      initMonitor();
      uiInfo();
      notifAction();
    };
    return game.count++;
  };
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                                Ending Game                                */
/*                                                                           */
/* - - - Some Win                                                            */
/* ************************************************************************* */
const endGame = (player) => {
  game.turnNumber = 3;
  notifier.textContent = `${player} win, press esc to play again`;
  flur(`${player} win, press esc to play again`);
  notifAction();
  uiColor();
  /*
  Listen for ESCAPE TOUCH && force to refresh !
  */
};

//*****************************************************************************
//*****************************************************************************
//********************************  A S S E T S  ******************************
//*****************************************************************************
//*****************************************************************************
//*****************************************************************************

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             INIT MAP SIZE                                 */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const setTable = () => {
  var table = document.getElementById('map');
  table.style.width = (30 * vd) + "px";
  table.style.height = (30 * vd) + "px";
  table.style.marginTop = ((ch * vd) - (18 * vd)) + "px";
  table.style.marginLeft = ((cw * vd) - (15 * vd)) + "px";

  /* var subTable = document.getElementById('subTable');
  subTable.style.width = (40 * vd) + "px";
  subTable.style.height = (30 * vd) + "px";
  subTable.style.marginTop = ((ch * vd) - (27 * vd)) + "px";
  subTable.style.marginLeft = ((cw * vd) - (19.8 * vd)) + "px"; */
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                    GET REAL MAP SIZE AFTER TRANSFORM                      */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const isoMapSize = () => {
  var MapSize = document.getElementById('map');

  mapHeight = MapSize.getBoundingClientRect().height;
  mapWidth = MapSize.getBoundingClientRect().width;
  isoInitTop =  ((ch * vd) - ((mapHeight /2) + (4.6 * vd)));
  isoInitLeft = ((cw * vd) - (1.6 * vd));

  playerSpritePos(playerOne, isoInitTop, isoInitLeft);
  playerSpritePos(playerTwo, isoInitTop, isoInitLeft);
  return [isoInitLeft, isoInitTop, mapWidth, mapHeight];
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                         PLACING PLAYER SPRITE                             */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const playerSpritePos = (player, Top, Left) => {
  var playerImg = document.getElementById(`${player.name}Img`);;
  playerImg.style.width = (3 * vd) +"px";
  playerImg.style.height = (3 * vd) +"px";
  playerImg.style.marginTop = Top + 'px';
  playerImg.style.marginLeft = Left + 'px';
  playerImg.style.zIndex = '4';
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                    POSITION SPRITE TO PLAYER POSITION                     */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const autoImgPos = (player) => {
  var xAxis;
  var yAxis;
  var plyPos;
  var imgPosY;
  var imgPosX;

  if (player.pos <= 9)
  {
    plyPos = '0'+player.pos;
  }
  else {
    plyPos = player.pos;
  }

  xAxis = Number((plyPos).toString()[1]);
  yAxis = Number((plyPos).toString()[0]);
  imgPosY = isoInitTop + ((mapHeight /20) * yAxis) + ((mapWidth /40) * xAxis);
  imgPosX = isoInitLeft - ((mapWidth /20) * yAxis) + ((mapHeight /10) * xAxis);

  var playerImg = document.getElementById(`${player.name}Img`);
  playerImg.style.marginTop = imgPosY + 'px';
  playerImg.style.marginLeft = imgPosX + 'px';
  playerImg.style.zIndex = yAxis + 1;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                    POSITION SPRITE for Rocked Cells                       */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const setRockImg = (rock) => {
  var xAxis;
  var yAxis;
  var rockPos;
  var imgPosY;
  var imgPosX;

  if (rock <= 9)
  {
    rockPos = '0'+ rock;
  }
  else {
    rockPos = rock;
  };

  xAxis = Number(rockPos.toString()[1]);
  yAxis = Number(rockPos.toString()[0]);
  imgPosY = isoInitTop + ((mapHeight /20) * yAxis) + ((mapWidth /40) * xAxis);
  imgPosX = isoInitLeft - ((mapWidth /20) * yAxis) + ((mapHeight /10) * xAxis);

  var rockSprite = document.createElement('img');
  rockSprite.style.width = (2.4 * vd) +"px";
  rockSprite.style.height = (2.4 * vd) +"px";
  rockSprite.style.marginTop = imgPosY + (0.88 * vd) + 'px';
  rockSprite.style.marginLeft = imgPosX + (0.38 * vd) + 'px';
  rockSprite.style.zIndex = yAxis + 0.5;
  rockSprite.src = "./assets/rock.svg";
  rockSprite.classList.add('rockedImg');

  return document.querySelector('body').appendChild(rockSprite);
};

//*****************************************************************************
const removeRockedSprite = () => {
  var c = document.body.getElementsByClassName('rockedImg');
  var x = c.length;
  let i1 = 0;
  let i2 = 0;

  while (i1 < x)
  {
    c = document.body.getElementsByClassName('rockedImg');
    i2 = 0;
    while (i2 < 5)
    {
      document.body.removeChild(c[i2]);
      if (!(typeof c === 'undefined'))
      {
        return removeRockedSprite();
      };
      i2++;
    };
    i1++;
  };
  return isMap.filter((e, i) => {if (hasClass(e, 'rocked')){setRockImg(i)}});
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                          Set Sprite for Weapons                           */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const setWeaponImg = (weap) => {
  var xAxis;
  var yAxis;
  var weapPos;
  var imgPosY;
  var imgPosX;

  if (weap <= 9)
  {
    weapPos = '0'+ weap;
  }
  else {
    weapPos = weap;
  };

  xAxis = Number(weapPos.toString()[1]);
  yAxis = Number(weapPos.toString()[0]);
  imgPosY = isoInitTop + ((mapHeight /20) * yAxis) + ((mapWidth /40) * xAxis);
  imgPosX = isoInitLeft - ((mapWidth /20) * yAxis) + ((mapHeight /10) * xAxis);

  var weapSprite = document.createElement('img');
  weapSprite.style.width = (2.5 * vd) +"px";
  weapSprite.style.height = (2.5 * vd) +"px";
  weapSprite.style.marginTop = imgPosY + (1.2 * vd) + 'px';
  weapSprite.style.marginLeft = imgPosX + (0.30 * vd) + 'px';
  weapSprite.style.zIndex = yAxis + 0.5;
  weapSprite.classList.add('weapImg');

  if (hasClass(isMap[weap], 'weap1'))
  {
    weapSprite.src = "./assets/wood.SVG";
    weapSprite.id = 'weapImg1';
  }
  else if (hasClass(isMap[weap], 'weap2'))
  {
    weapSprite.src = "./assets/sword.SVG";
    weapSprite.id = 'weapImg2';
  }
  else if (hasClass(isMap[weap], 'weap3'))
  {
    weapSprite.src = "./assets/shuriken.SVG";
    weapSprite.id = 'weapImg3';
  }
  else if (hasClass(isMap[weap], 'weap4'))
  {
    weapSprite.src = "./assets/spear.SVG";
    weapSprite.id = 'weapImg4';
  }
  else
  {
    weapSprite.src = "./assets/sword.SVG";
  };

  return document.querySelector('body').appendChild(weapSprite);
};

//*****************************************************************************
const removeWeaponSprite = () => {
  var c = document.body.getElementsByClassName('weapImg');
  var x = c.length;
  let i1 = 0;
  let i2 = 0;

  while (i1 < x)
  {
    c = document.body.getElementsByClassName('weapImg');
    i2 = 0;
    while (i2 < 5)
    {
      document.body.removeChild(c[i2]);
      if (!(typeof c === 'undefined'))
      {
        return removeWeaponSprite();
      };
      i2++;
    };
    i1++;
  };
  return isMap.filter((e, i) => {if (hasClass(e, 'weapon')){setWeaponImg(i)}});
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               Monitor UI                                  */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const initMonitor = () => {
  var buttonColorChanger = document.getElementsByClassName('button');

  var monitorHTML = document.getElementById('monitor');
  monitorHTML.style.width = 22 * vd + 'px';
  monitorHTML.style.height = 8.5 * vd + 'px';
  monitorHTML.style.marginTop = (ch * vd) + (7 * vd) + 'px';
  monitorHTML.style.marginLeft = (cw * vd) - (11 * vd) + 'px';

  var subMonitorHTML = document.getElementById('subMonitor');
  subMonitorHTML.style.width = 22 * vd + 'px';
  subMonitorHTML.style.height = 8.5 * vd + 'px';
  subMonitorHTML.style.marginTop = (ch * vd) + (7.5 * vd) + 'px';
  subMonitorHTML.style.marginLeft = (cw * vd) - (11 * vd) + 'px';

  document.getElementById('zone_time').style.fontSize = 1.2 * vd +'px';
  document.getElementById('zone_turn').style.fontSize = 1.2 * vd +'px';
  document.getElementById('zone_life').style.fontSize = 1.2 * vd +'px';
  document.getElementById('zone_weapon').style.fontSize = 1.2 * vd +'px';

  uiColor();
  initButton();
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               Button UI                                   */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const initButton = () => {
  var buttonPos= document.getElementsByClassName('button');
  var subButtonPos= document.getElementsByClassName('subButton');

  buttonPos[0].style.marginTop = (ch * vd) + (11 * vd) + 'px';
  buttonPos[0].style.marginLeft = (cw * vd) - (22 * vd) + 'px';
  buttonPos[1].style.marginTop = (ch * vd) + (7.5 * vd) + 'px';
  buttonPos[1].style.marginLeft = (cw * vd) - (18 * vd) + 'px';
  buttonPos[2].style.marginTop = (ch * vd) + (7.5 * vd) + 'px';
  buttonPos[2].style.marginLeft = (cw * vd) + (14 * vd) + 'px';
  buttonPos[3].style.marginTop = (ch * vd) + (11 * vd) + 'px';
  buttonPos[3].style.marginLeft = (cw * vd) + (18 * vd) + 'px';

  subButtonPos[0].style.marginTop = (ch * vd) + (11.3 * vd) + 'px';
  subButtonPos[0].style.marginLeft = (cw * vd) - (22 * vd) + 'px';
  subButtonPos[1].style.marginTop = (ch * vd) + (7.8 * vd) + 'px';
  subButtonPos[1].style.marginLeft = (cw * vd) - (18 * vd) + 'px';
  subButtonPos[2].style.marginTop = (ch * vd) + (7.8 * vd) + 'px';
  subButtonPos[2].style.marginLeft = (cw * vd) + (14 * vd) + 'px';
  subButtonPos[3].style.marginTop = (ch * vd) + (11.3 * vd) + 'px';
  subButtonPos[3].style.marginLeft = (cw * vd) + (18 * vd) + 'px';
};

//*****************************************************************************
const uiColor = () => {
  var monitorHTML = document.getElementById('monitor');
  var subButton = document.getElementsByClassName('subButton');
  var subMonitorHTML = document.getElementById('subMonitor');
  var buttonColorChanger = document.getElementsByClassName('button');

  if (game.turnNumber === 1)
  {
    monitorHTML.style.backgroundColor = "#EB7424";
    subMonitorHTML.style.backgroundColor = "#B15716";

    buttonColorChanger[0].style.backgroundColor = "#EB7424";
    buttonColorChanger[1].style.backgroundColor = "#EB7424";
    buttonColorChanger[2].style.backgroundColor = "#EB7424";
    buttonColorChanger[3].style.backgroundColor = "#EB7424";

    subButton[0].style.backgroundColor = "#B15716";
    subButton[1].style.backgroundColor = "#B15716";
    subButton[2].style.backgroundColor = "#B15716";
    subButton[3].style.backgroundColor = "#B15716";

  }
  else if (game.turnNumber === 2) {
    monitorHTML.style.backgroundColor = "#50A4C6";
    subMonitorHTML.style.backgroundColor = "#306A81";
    buttonColorChanger[0].style.backgroundColor = "#50A4C6";
    buttonColorChanger[1].style.backgroundColor = "#50A4C6";
    buttonColorChanger[2].style.backgroundColor = "#50A4C6";
    buttonColorChanger[3].style.backgroundColor = "#50A4C6";

    subButton[0].style.backgroundColor = "#306A81";
    subButton[1].style.backgroundColor = "#306A81";
    subButton[2].style.backgroundColor = "#306A81";
    subButton[3].style.backgroundColor = "#306A81";
  }
  else if (game.turnNumber === 3) {
    monitorHTML.style.backgroundColor = "#666464";
    subMonitorHTML.style.backgroundColor = "#393838";
    buttonColorChanger[0].style.backgroundColor = "#666464";
    buttonColorChanger[1].style.backgroundColor = "#666464";
    buttonColorChanger[2].style.backgroundColor = "#666464";
    buttonColorChanger[3].style.backgroundColor = "#666464";

    subButton[0].style.backgroundColor = "#393838";
    subButton[1].style.backgroundColor = "#393838";
    subButton[2].style.backgroundColor = "#393838";
    subButton[3].style.backgroundColor = "#393838";
  };
  return;
};

//*****************************************************************************
const getButTop = (button) => {
  var top = window.getComputedStyle(button).getPropertyValue("margin-top");
  return (top);
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                          reset UI Information                             */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const uiInfo = () => {
  var uiTurn = document.getElementById('zone_turn');
  var uiWeapon = document.getElementById('zone_weapon');
  var uiLife = document.getElementById('zone_life');

  if (game.turnNumber == 1)
  {
    uiTurn.textContent = `One`;
    uiWeapon.textContent = `${playerOne.arme}`;
    uiLife.textContent = `${playerOne.vie}`;
  }
  else
  {
    uiTurn.textContent = `Two`;
    uiWeapon.textContent = `${playerTwo.arme}`;
    uiLife.textContent = `${playerTwo.vie}`;
  };
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                              NOTIFIER UI                                  */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const notifUi = () => {
  notifier.style.fontSize = 1.2 * vd +'px';
  notifier.style.width = 30 * vd + 'px';
  notifier.style.marginLeft = (cw * vd) - (15 * vd) + 'px';
  notifier.style.marginTop = (ch * vd) + (4 * vd) + 'px';
};

const notifAction = () => {
  notifier.style.opacity = 1;
  if (game.turnNumber < 3)
  {
    setTimeout(() => notifier.style.opacity = 0, 2200);
  };
  return;
};

//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               init game                                   */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
getMap();
initMonitor();
getButAtt = getButTop(but_attack);
getButMv = getButTop(but_move);
getButDef = getButTop(but_defend);
getButTrn = getButTop(but_turn);
setRock();
setWeapon();
setPlayer(playerOne);
setPlayer(playerTwo);
checkStartPos(playerOne, playerTwo);
setTurn(game);
doTurn();
uiInfo();
notifUi();

/*
subTable have to be remake ? keep or not ?

END GAME()--> Listen for esc && force refresh
Smaller shuriken
Issue with class modification of weapon line 665
Issue with weapon sometime got only 3 / checkWeapon();
Check if Notif is Ok -- Last Notif  bug !!!
Should i create a special Node with the last message ???
do separate script
Index.Js made of getScript('js/????.js') then function call every initializator;
*/
